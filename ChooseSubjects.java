package com.example.admin.demo;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ChooseSubjects extends AppCompatActivity {

    ImageView math;
    ImageView physic;
    ImageView chemistry;
    ImageView biology;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_subjects);
        math = findViewById(R.id.MathImage);
        physic = findViewById(R.id.PhysicImage);
        chemistry = findViewById(R.id.ChemistryImage);
        biology = findViewById(R.id.BiologyImage);

        math.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent1 =new Intent(ChooseSubjects.this, TakePhoto.class);
                startActivity(myIntent1);
            }
        });
        physic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent1 =new Intent(ChooseSubjects.this, TakePhoto.class);
                startActivity(myIntent1);
            }
        });
        chemistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent1 =new Intent(ChooseSubjects.this, TakePhoto.class);
                startActivity(myIntent1);
            }
        });
        biology.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent1 =new Intent(ChooseSubjects.this, TakePhoto.class);
                startActivity(myIntent1);
            }
        });
    }
}
