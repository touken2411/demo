package com.example.admin.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button loginButton;
    TextView signupText;
    TextView forgotpasswordText;
    EditText usernameText;
    EditText passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        loginButton = findViewById(R.id.buttonLogin);
        signupText = findViewById(R.id.textSignup);
        forgotpasswordText=findViewById(R.id.textForgotPassword);
        usernameText = findViewById(R.id.textUsername);
        passwordText = findViewById(R.id.textPassword);



        signupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent myIntent =new Intent(MainActivity.this, StudentSignUp.class);
                startActivity(myIntent);
                //doLogin(username, password);
                //Toast.makeText(getApplicationContext(), "Username: " + username + " password: " + password, Toast.LENGTH_LONG).show();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameText.getText().toString();
                String password = passwordText.getText().toString();

                if(username.equalsIgnoreCase(""))
                {
                    usernameText.setHint("Please enter UserName!");
                    usernameText.setError("Please enter UserName!");
                }
                else if (password.equalsIgnoreCase(""))
                {
                    passwordText.setHint("Please enter Password!");
                    passwordText.setError("Please enter Password!");
                }
                else{
                    //qua trang chọn môn
                    Intent myIntent1 =new Intent(MainActivity.this, ChooseSubjects.class);
                    startActivity(myIntent1);
                }

            }
        });

        forgotpasswordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameText.getText().toString();
                String password = passwordText.getText().toString();

                //qua trang điều chỉnh lại password
            }
        });
    }


}
