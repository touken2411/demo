package com.example.admin.demo1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterStudent extends AppCompatActivity {

    Button LoginButton;
    EditText UserName;
    EditText Password;
    EditText Gmail;
    EditText ConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_student);

        LoginButton =findViewById(R.id.buttonLogin);
        UserName =findViewById(R.id.textUsername);
        Gmail=findViewById(R.id.textGmail);
        Password=findViewById(R.id.textPassword);
        ConfirmPassword=findViewById(R.id.textConfirmPassword);



        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //kt xem đã nhập đầy đủ thông tin hay chưa
                String username = UserName.getText().toString();
                String password = Password.getText().toString();
                String gmail = Gmail.getText().toString();
                String confirmPassword= ConfirmPassword.getText().toString();

                if (username.equalsIgnoreCase("")) {
                    UserName.setHint("Please enter UserName!");
                    UserName.setError("Please enter UserName!");
                } else if (gmail.equalsIgnoreCase("")) {
                    Gmail.setHint("Please enter Password!");
                    Gmail.setError("Please enter Password!");
                } else if (password.equalsIgnoreCase("")) {
                    Password.setHint("Please enter Password!");
                    Password.setError("Please enter Password!");
                } else if (password.equalsIgnoreCase("")) {
                    ConfirmPassword.setHint("Please enter ConfirmPassword!");
                    ConfirmPassword.setError("Please enter ConfirmPassword!");

                } else if (!password.equals(confirmPassword)) {
                    Toast.makeText(RegisterStudent.this, "Password does not match", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent myIntent = new Intent(RegisterStudent.this, ChooseSubjects.class);
                    startActivity(myIntent);
                }

            }

        });
    }

}
