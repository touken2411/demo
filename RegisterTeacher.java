package com.example.raymond.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.FileNotFoundException;

public class register extends AppCompatActivity {

    EditText hoGV;
    EditText tenGV;
    EditText CMNDGV;
    EditText emailGV;
    EditText soDienThoaiGV;
    EditText chuyenMon;
    Button hinhAnhGV;
    ImageView avatar;

    public static final int PICK_IMAGE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        hoGV = findViewById(R.id.hoGV);
        tenGV = findViewById(R.id.tenGV);
        CMNDGV = findViewById(R.id.CMNDGV);
        emailGV = findViewById(R.id.emailGV);
        soDienThoaiGV = findViewById(R.id.soDienThoaiGV);
        chuyenMon = findViewById(R.id.chuyenMon);
        //hinhAnhGV = findViewById(R.id.buttonTakePic);
        avatar = findViewById(R.id.imageAvatar);

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);

            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE) {
            // Let's read picked image data - its URI
            Uri pickedImage = data.getData();
            // Let's read picked image path using content resolver
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(pickedImage));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            // Do something with the bitmap
            avatar.setImageBitmap(bitmap);

        }
    }
}
