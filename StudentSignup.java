package com.example.admin.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StudentSignUp extends AppCompatActivity {

    Button LoginButton;
    EditText UserName;
    EditText Password;
    EditText FullName;
    EditText Gmail;
    EditText Phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_sign_up);

        LoginButton =findViewById(R.id.buttonLogin);
        UserName =findViewById(R.id.textUsername);
        Password=findViewById(R.id.textPassword);
        FullName=findViewById(R.id.textFullName);
        Gmail=findViewById(R.id.textGmail);
        Phone=findViewById(R.id.textPhone);

        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //kt xem đã nhập đầy đủ thông tin hay chưa
                String username = UserName.getText().toString();
                String passwword = Password.getText().toString();
                String fullname = FullName.getText().toString();
                String gmail = Gmail.getText().toString();
                String phone = Phone.getText().toString();

                if(username.equalsIgnoreCase(""))
                {
                    UserName.setHint("Please enter UserName!");
                    UserName.setError("Please enter UserName!");
                }
                else if (passwword.equalsIgnoreCase(""))
                {
                    Password.setHint("Please enter Password!");
                    Password.setError("Please enter Password!");
                }
                else if (fullname.equalsIgnoreCase(""))
                {
                    FullName.setHint("Please enter FullName!");
                    FullName.setError("Please enter FullName!");
                }
                else if (gmail.equalsIgnoreCase(""))
                {
                    Gmail.setHint("Please enter Password!");
                    Gmail.setError("Please enter Password!");
                }
                else if (phone.equalsIgnoreCase(""))
                {
                    Phone.setHint("Please enter Password!");
                    Phone.setError("Please enter Password!");
                }
                else
                {
                    //chuyển sang trang chọn môn
                    Intent myIntent =new Intent(StudentSignUp.this, ChooseSubjects.class);
                    startActivity(myIntent);
                }

            }
        });
    }
}
